<?php

namespace App\Controller;

use App\Entity\Organisation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(EntityManagerInterface $manager): Response
    {
        /** @var Organisation[] $organisations */
        $organisations = $manager->getRepository(Organisation::class)->findAll();
        $sorted = [];
        foreach ($organisations as $organisation) {
            $sorted[$organisation->getName()] = $organisation;
        }

        ksort($sorted);

        return $this->render('pages/home.html.twig', ['organisations' => $sorted, 'amount' => count($sorted)]);
    }


    /**
     * @Route("/list")
     * @return Response
     */
    public function list(EntityManagerInterface $manager): Response
    {
        $organisations = $manager->getRepository(Organisation::class)->findAll();

        return $this->render('pages/list.html.twig', ['organisations' => $organisations]);
    }
}
