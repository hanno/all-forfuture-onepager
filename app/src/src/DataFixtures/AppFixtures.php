<?php

namespace App\DataFixtures;

use App\Entity\Organisation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use League\Csv\Reader;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $reader = Reader::createFromPath(__DIR__.'/../Repository/data/organisations.csv');
        $reader->setHeaderOffset(0);
        $reader->setEnclosure('"');
        $reader->setDelimiter(',');
        /** @var \Iterator $results */
        $results = $reader->getRecords(['name', 'link', 'logo', 'mail', 'description']);
        foreach ($results as $row) {
            $organisation = new Organisation();
            $organisation->setName($row['name']);
            $organisation->setDescription($row['description']);
            $organisation->setLink($row['link']);
            if (!empty($row['logo'])) {
                $pathInfo = pathinfo($row['logo']);
                if (file_exists(__DIR__.'/../../public/img/logos/'.$pathInfo['basename'])) {
                    $organisation->setLogoUrl('/img/logos/'.$pathInfo['basename']);
                }

            }
            $manager->persist($organisation);
        }

        $manager->flush();
    }
}
